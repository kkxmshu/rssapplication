package com.example.dmitriy.rssapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dmitriy on 14.11.16.
 */

public class RSSItem implements Parcelable {

    private String title;
    private String description;
    private String pubDate;
    private String category;
    private String link;

    public RSSItem(String title, String description, String pubDate, String category, String link){
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
        this.category = category;
        this.link = link;
    }

    RSSItem(Parcel in) {
        title = in.readString();
        description = in.readString();
        pubDate = in.readString();
        category = in.readString();
        link = in.readString();
    }

    public static final Creator<RSSItem> CREATOR = new Creator<RSSItem>() {
        @Override
        public RSSItem createFromParcel(Parcel in) {
            return new RSSItem(in);
        }

        @Override
        public RSSItem[] newArray(int size) {
            return new RSSItem[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getCategory() {
        return category;
    }

    public String getLink() {
        return link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(pubDate);
        parcel.writeString(category);
        parcel.writeString(link);
    }

}
