package com.example.dmitriy.rssapplication.service;

/**
 * Created by dmitriy on 14.11.16.
 */

public interface DataLoadListener {

    void onDataLoaded(String str);

}
