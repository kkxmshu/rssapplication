package com.example.dmitriy.rssapplication.activity;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import java.util.ArrayList;
import com.example.dmitriy.rssapplication.R;
import com.example.dmitriy.rssapplication.adapter.RecyclerAdapter;
import com.example.dmitriy.rssapplication.adapter.RecyclerItemListener;
import com.example.dmitriy.rssapplication.model.RSSItem;
import com.example.dmitriy.rssapplication.service.DataLoaderIntentService;
import com.example.dmitriy.rssapplication.utils.Constants;
import com.example.dmitriy.rssapplication.utils.NetworkUtil;

public class MainActivity extends AppCompatActivity implements RecyclerItemListener, SwipeRefreshLayout.OnRefreshListener {

    private Uri uri = Uri.parse(Constants.PROVIDER_URI);
    private RecyclerAdapter adapter;
    private ArrayList<RSSItem> items = new ArrayList<>();
    private SwipeRefreshLayout swRefreshLayout;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecycler();
        initSwipeRefreshLayout();
        initBroadcastReceiver();
        checkNetworkState();
    }

    private void initSwipeRefreshLayout() {
        swRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swRefreshLayout);
        swRefreshLayout.setOnRefreshListener(this);
        swRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swRefreshLayout.setRefreshing(true);
            }
        });
    }

    public void insert_items(ArrayList<RSSItem> items) {
        for (RSSItem item : items){
            ContentValues cv = new ContentValues();
            cv.put(Constants.TITLE, item.getTitle());
            cv.put(Constants.DESCRIPTION, item.getDescription());
            cv.put(Constants.PUB_DATE, item.getPubDate());
            cv.put(Constants.CATEGORY, item.getCategory());
            cv.put(Constants.LINK, item.getLink());
            Uri newUri = getContentResolver().insert(uri, cv);
            Log.d(Constants.LOG_TAG, "insert, result Uri : " + newUri.toString());
        }
    }

    public void delete_items() {
        ContentUris.withAppendedId(uri, 3);
        int cnt = getContentResolver().delete(uri, null, null);
        Log.d(Constants.LOG_TAG, "delete, count = " + cnt);
    }

    public void query_items(){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor.moveToFirst()){
            items.clear();
            do {
                String title = cursor.getString(cursor.getColumnIndex(Constants.TITLE));
                String description = cursor.getString(cursor.getColumnIndex(Constants.DESCRIPTION));
                String category = cursor.getString(cursor.getColumnIndex(Constants.CATEGORY));
                String pub_date = cursor.getString(cursor.getColumnIndex(Constants.PUB_DATE));
                String link = cursor.getString(cursor.getColumnIndex(Constants.LINK));
                RSSItem item = new RSSItem(title, description, category, pub_date, link);
                items.add(item);
            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    private void initRecycler() {
        RecyclerView rvRSSItems = (RecyclerView)findViewById(R.id.rvRssItems);
        adapter = new RecyclerAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvRSSItems.setLayoutManager(linearLayoutManager);
        rvRSSItems.setAdapter(adapter);
    }

    private void initBroadcastReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                items.clear();
                items = intent.getParcelableArrayListExtra(Constants.LIST_ITEMS);
                delete_items();
                insert_items(items);
                adapter.update(items);
                swRefreshLayout.setRefreshing(false);
            }
        };
        IntentFilter filter = new IntentFilter(Constants.INTENT_LIST_ITEMS_KEY);
        registerReceiver(receiver, filter);
    }

    /** here we check the internet an if it turned on -
     * start service to load network data
     * and if it turned off - get it from local database */

    private void checkNetworkState() {
        NetworkUtil networkUtil = new NetworkUtil();
        if (networkUtil.checkNetwork()){
            Intent intent = new Intent(MainActivity.this, DataLoaderIntentService.class);
            intent.putExtra(Constants.HABRA_URL, Constants.BASE_URL);
            startService(intent);
        } else {
            query_items();
            adapter.update(items);
            swRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swRefreshLayout.setRefreshing(false);
                }
            });
            Toast.makeText(this, "no internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(MainActivity.this, DataLoaderIntentService.class);
        stopService(intent);
        unregisterReceiver(receiver);
    }

    @Override
    public void onItemClicked(String itemLink) {
        Intent intent = new Intent(this, WebActivity.class);
        intent.putExtra(Constants.LINK, itemLink);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        checkNetworkState();
    }
}
