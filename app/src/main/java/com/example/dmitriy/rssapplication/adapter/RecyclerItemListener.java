package com.example.dmitriy.rssapplication.adapter;

/**
 * Created by dmitriy on 14.11.16.
 */

public interface RecyclerItemListener {

    void onItemClicked(String itemLink);

}
