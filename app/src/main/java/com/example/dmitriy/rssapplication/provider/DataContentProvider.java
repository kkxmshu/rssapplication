package com.example.dmitriy.rssapplication.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.example.dmitriy.rssapplication.utils.Constants;

/**
 * Created by dmitriy on 14.11.16.
 */

public class DataContentProvider extends ContentProvider {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(Constants.AUTHORITY, Constants.PATH, Constants.URI_ITEMS);
        uriMatcher.addURI(Constants.AUTHORITY, Constants.PATH + "/#", Constants.URI_ITEM);
    }

    @Override
    public boolean onCreate() {
        Log.d(Constants.LOG_TAG, "onCreate");
        dbHelper = new DBHelper(getContext());
        return true;
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.d(Constants.LOG_TAG, "query, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case Constants.URI_ITEMS:
                Log.d(Constants.LOG_TAG, "URI_CONTACTS");
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = Constants.TITLE + " ASC";
                }
                break;
            case Constants.URI_ITEM:
                String id = uri.getLastPathSegment();
                Log.d(Constants.LOG_TAG, "URI_CONTACTS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = Constants.URI_ITEM + " = " + id;
                } else {
                    selection = selection + " AND " + Constants.URI_ITEM + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(Constants.TABLE_NAME, projection,
                selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), Constants.RSS_CONTENT_URI);
        return cursor;
    }

    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Log.d(Constants.LOG_TAG, "insert, " + uri.toString());
        if (uriMatcher.match(uri) != Constants.URI_ITEMS)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        db = dbHelper.getWritableDatabase();
        long rowID = db.insert(Constants.TABLE_NAME, null, values);
        Log.d(Constants.LOG_TAG, "insert, rowID = " + rowID);
        Uri resultUri = ContentUris.withAppendedId(Constants.RSS_CONTENT_URI, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        Log.d(Constants.LOG_TAG, "delete, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case Constants.URI_ITEMS:
                Log.d(Constants.LOG_TAG, "URI_CONTACTS");
                break;
            case Constants.URI_ITEM:
                String id = uri.getLastPathSegment();
                Log.d(Constants.LOG_TAG, "URI_CONTACTS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = Constants.URI_ITEM + " = " + id;
                } else {
                    selection = selection + " AND " + Constants.URI_ITEMS + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.delete(Constants.TABLE_NAME, selection, selectionArgs);
        Log.d(Constants.LOG_TAG, "delete, cnt = " + cnt);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        Log.d(Constants.LOG_TAG, "getType, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case Constants.URI_ITEMS:
                return Constants.RSS_CONTENT_TYPE;
            case Constants.URI_ITEM:
                return Constants.RSS_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    private class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context) {
            super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Constants.DB_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

}
