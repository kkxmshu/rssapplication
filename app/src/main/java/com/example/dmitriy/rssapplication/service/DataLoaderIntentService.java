package com.example.dmitriy.rssapplication.service;

import org.w3c.dom.Document;
import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.example.dmitriy.rssapplication.parser.XMLParser;
import com.example.dmitriy.rssapplication.model.RSSItem;
import com.example.dmitriy.rssapplication.utils.Constants;
import java.util.ArrayList;

/**
 * Created by dmitriy on 14.11.16.
 */

public class DataLoaderIntentService extends IntentService implements DataLoadListener {

    private XMLParser parser;

    public DataLoaderIntentService() {
        super(Constants.INTENT_SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String url = intent.getStringExtra(Constants.HABRA_URL);
        parser = new XMLParser(this);
        parser.getXmlFromUrl(url);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDataLoaded(String str) {
        Document dom = parser.getDomElement(str);
        ArrayList<RSSItem> items = parser.getItemsList(dom);
        Intent intent = new Intent(Constants.INTENT_LIST_ITEMS_KEY);
        intent.putParcelableArrayListExtra(Constants.LIST_ITEMS, items);
        sendBroadcast(intent);
    }
}
