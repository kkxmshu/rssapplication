package com.example.dmitriy.rssapplication.parser;

import android.util.Log;
import com.example.dmitriy.rssapplication.service.DataLoadListener;
import com.example.dmitriy.rssapplication.model.RSSItem;
import com.example.dmitriy.rssapplication.utils.Constants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLParser {

	private final DataLoadListener listener;

	public XMLParser(DataLoadListener listener) {
		this.listener = listener;
	}

	public void getXmlFromUrl(String url) {
		String xml = null;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			xml = EntityUtils.toString(httpEntity);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		listener.onDataLoaded(xml);
	}

	public Document getDomElement(String xml){
		Document doc;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setCoalescing(true);
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
		        is.setCharacterStream(new StringReader(xml));
		        doc = db.parse(is);
			} catch (ParserConfigurationException e) {
				Log.e("Error: ", e.getMessage());
				return null;
			} catch (SAXException e) {
				Log.e("Error: ", e.getMessage());
	            return null;
			} catch (IOException e) {
				Log.e("Error: ", e.getMessage());
				return null;
			}
	        return doc;
	}

	 private String getElementValue(Node elem ) {
	     Node child;
	     if( elem != null){
	         if (elem.hasChildNodes()){
	             for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
	                 if( child.getNodeType()  == Node.CDATA_SECTION_NODE || child.getNodeType() == Node.TEXT_NODE  ){
	                     return child.getNodeValue();
	                 }
	             }
	         }
	     }
	     return "";
	 }

	private String getValue(Element item, String str) {
			NodeList n = item.getElementsByTagName(str);
			return this.getElementValue(n.item(0));
	 }

	public ArrayList<RSSItem> getItemsList(Document doc) {
		NodeList nl = doc.getElementsByTagName(Constants.RSS_ITEM);
		ArrayList<RSSItem> list = new ArrayList<>();
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);
			String title = getValue(e, Constants.TITLE);
			String description = getValue(e, Constants.DESCRIPTION);
			String pubDate = getValue(e, Constants.PUB_DATE);
			String category = getValue(e, Constants.CATEGORY);
			String link = getValue(e, Constants.LINK);
			RSSItem item = new RSSItem(title, description, pubDate, category, link);
			list.add(item);
		}
		return list;
	}

}
