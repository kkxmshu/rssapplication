package com.example.dmitriy.rssapplication;

import android.app.Application;

/**
 * Created by dmitriy on 14.11.16.
 */

public class MainApplication extends Application {

    private static MainApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static MainApplication getInstance() {
        if (instance == null)
            return new MainApplication();
        else
            return instance;
    }
}