package com.example.dmitriy.rssapplication.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.dmitriy.rssapplication.MainApplication;

/**
 * Created by dmitriy on 14.11.16.
 */

public class NetworkUtil {

    public boolean checkNetwork(){
        Context context = MainApplication.getInstance();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {
            return true;
        } else {
            return false;
        }
    }

}
