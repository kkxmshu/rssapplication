package com.example.dmitriy.rssapplication.utils;

import android.net.Uri;

/**
 * Created by dmitriy on 14.11.16.
 */

public class Constants {
    public static final String LOG_TAG = "myLogs";
    public static final String PROVIDER_URI = "content://com.example.dmitriy.rssapplication.ItemsBook/items";
    public static final String HABRA_URL = "habra_url";
    public static final String BASE_URL = "https://habrahabr.ru/rss/interesting/";
    public static final String RSS_ITEM = "item";

    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String PUB_DATE = "pubDate";
    public static final String CATEGORY = "category";
    public static final String DESCRIPTION = "description";
    public static final String INTENT_LIST_ITEMS_KEY = "intent_list_items_key";
    public static final String LIST_ITEMS = "list_items";
    public static final String DB_NAME = "database_name";
    public static final String TABLE_NAME = "table_name";
    public static final int URI_ITEMS = 1;
    public static final int URI_ITEM = 2;

    public static final int DB_VERSION = 1;
    public static final String DB_CREATE = "create table "
            + TABLE_NAME + "("
            + "_id integer primary key autoincrement, "
            + TITLE + " title, "
            + DESCRIPTION + " description, "
            + PUB_DATE + " pubDate, "
            + CATEGORY + " category, "
            + LINK + " link);";

    public static final String AUTHORITY = "com.example.dmitriy.rssapplication.ItemsBook";
    public static final String PATH = "items";

    // the common uri
    public static final Uri RSS_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + PATH);

    // set of items
    public static final String RSS_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + PATH;

    // one item
    public static final String RSS_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + PATH;

    public static final String INTENT_SERVICE_NAME = "intent_service_name";
}
