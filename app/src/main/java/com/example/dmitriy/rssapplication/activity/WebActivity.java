package com.example.dmitriy.rssapplication.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;
import com.example.dmitriy.rssapplication.R;
import com.example.dmitriy.rssapplication.web.RSSWebViewClient;
import com.example.dmitriy.rssapplication.utils.Constants;

public class WebActivity extends AppCompatActivity {

    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        readIntent();
        initView();
    }

    private void initView() {
        WebView webView = (WebView)findViewById(R.id.webView);
        webView.setWebViewClient(new RSSWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        if (link != null && !link.isEmpty()){
            webView.loadUrl(link);
        } else
            Toast.makeText(this, "no valid link", Toast.LENGTH_SHORT).show();
    }

    private void readIntent() {
        Intent intent = getIntent();
        link = intent.getStringExtra(Constants.LINK);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }
}
