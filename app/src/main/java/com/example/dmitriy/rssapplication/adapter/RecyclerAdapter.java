package com.example.dmitriy.rssapplication.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.dmitriy.rssapplication.MainApplication;
import com.example.dmitriy.rssapplication.R;
import com.example.dmitriy.rssapplication.model.RSSItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitriy on 14.11.16.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private MainApplication context;
    private List<RSSItem> list;
    private RecyclerItemListener listener;

    public RecyclerAdapter(RecyclerItemListener listener) {
        this.list = new ArrayList<>();
        this.listener = listener;
        this.context = MainApplication.getInstance();
    }

    public void update(List<RSSItem> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (!list.get(position).getTitle().equals(""))
            holder.tvTitle.setText(list.get(position).getTitle());
        if (!list.get(position).getDescription().equals("")){
            String description = list.get(position).getDescription();
            holder.tvDescription.setText(Html.fromHtml(description));
        }
        if (!list.get(position).getPubDate().equals(""))
            holder.tvDate.setText(list.get(position).getPubDate());
        if (!list.get(position).getCategory().equals(""))
            holder.tvCategory.setText(list.get(position).getCategory());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClicked(list.get(position).getLink());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        TextView tvTitle;
        TextView tvDescription;
        TextView tvDate;
        CardView cvReminder;
        TextView tvCategory;

        ViewHolder(View v) {
            super(v);
            view = v;
            tvTitle = (TextView)view.findViewById(R.id.tvTitle);
            tvDescription = (TextView)view.findViewById(R.id.tvDescription);
            tvDate = (TextView)view.findViewById(R.id.tvDate);
            cvReminder = (CardView)view.findViewById(R.id.cvReminder);
            tvCategory = (TextView)view.findViewById(R.id.tvCategory);
        }
    }
}